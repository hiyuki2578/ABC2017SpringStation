package jagsc.org.abc2017SpringStation;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by takan on 2017/05/18.
 */

public class EkiSQLiteOpenHelper extends SQLiteOpenHelper {
    Context context;

    public EkiSQLiteOpenHelper(Context context) {
        super(context, "station.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `station` (" +
                "  `station_cd` int(11) NOT NULL," +
                "  `station_g_cd` int(11) NOT NULL," +
                "  `station_name` varchar(80)  DEFAULT NULL," +
                "  `station_name_k` varchar(255)  DEFAULT NULL," +
                "  `station_name_r` varchar(255)  DEFAULT NULL," +
                "  `line_cd` int(11) NOT NULL," +
                "  `pref_cd` int(11) DEFAULT NULL," +
                "  `post` varchar(10)  DEFAULT NULL," +
                "  `add` varchar(255)  DEFAULT NULL," +
                "  `lon` decimal(9,6) DEFAULT NULL," +
                "  `lat` decimal(8,6) DEFAULT NULL," +
                "  `open_ymd` datetime DEFAULT NULL," +
                "  `close_ymd` datetime DEFAULT NULL," +
                "  `e_status` smallint(6) DEFAULT NULL," +
                "  `e_sort` int(11) DEFAULT NULL," +
                "  PRIMARY KEY (`station_cd`)" +
                ")");
        db.execSQL("CREATE TABLE `pref` (" +
                "  `pref_cd` int(11) NOT NULL," +
                "  `pref_name` varchar(255) DEFAULT NULL," +
                "  PRIMARY KEY (`pref_cd`)" +
                ")");
        db.execSQL("CREATE TABLE `join` (" +
                "  `line_cd` int(11) NOT NULL," +
                "  `station_cd1` int(11) NOT NULL," +
                "  `station_cd2` int(11) NOT NULL" +
                ")");
        importFromFile(db,"station","station20170403free.csv");
        importFromFile(db,"pref","pref.csv");
        importFromFile(db,"join","join20170403.csv");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void importFromFile(SQLiteDatabase db,String tableName,String fileName){
        try {
            InputStream is = this.context.getAssets().open(fileName);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(is,"UTF-8"));
            db.beginTransaction();
            String line = buffer.readLine();
            String[] fields = line.split(",");
            while ((line = buffer.readLine()) != null) {
                String[] str = line.split(",");
                ContentValues values = new ContentValues();
                for(int i = 0;i<fields.length;i++){
                    values.put("`" + fields[i] + "`",str[i]);
                }
                db.insert("`" + tableName + "`", null, values);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
