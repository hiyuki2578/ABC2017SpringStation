package jagsc.org.abc2017SpringStation;

import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.InputStream;

import jagsc.org.abc2017SpringStation.resp.RankTable;

public class SetTextActivity extends AppCompatActivity implements View.OnClickListener {
    private final int MP = TableLayout.LayoutParams.MATCH_PARENT;

    private Button start;
    public String station_name;
    public String station_time;
    private PlaySound PlaySound = new PlaySound();

    private TextView rankingTxtView[] = new TextView[3];
    private TextView licenseTxtView;
    private ScrollView licenseScrollView;
    private String licenseStr="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_text);

        LinearLayout activityLinearLayout = (LinearLayout)findViewById(R.id.activity_set_text);
        activityLinearLayout.setClickable(true);
        activityLinearLayout.setOnClickListener(this);

        start = (Button) findViewById(R.id.button_start);
        start.setOnClickListener(this);

        rankingTxtView[0] =(TextView)findViewById(R.id.rank1_text);
        rankingTxtView[1] =(TextView)findViewById(R.id.rank2_text);
        rankingTxtView[2] =(TextView)findViewById(R.id.rank3_text);
        licenseTxtView = (TextView)findViewById(R.id.license_text);
        licenseTxtView.setOnClickListener(this);
        licenseStr =getString(R.string.licenses);
        licenseScrollView = (ScrollView)findViewById(R.id.license_scroll_view);

    }

    @Override
    public void onResume() {
        super.onResume();
        PlaySound.initSe(this);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        PlaySound.PlayMusic(this, R.raw.start_bgm);

        //ランキング表示
        ScoreCommunicator scorecom = new ScoreCommunicator();
        scorecom.getrank("ranking",new ScoreCommunicator.GetRankListener(){
            @Override
            public void onResponse(RankTable ranktable) {
                //TODO:addViewで複数要素追加すると落ちるので対策
                String user_id_text = "";
                String point_text = "";
                int rankLen = ranktable.Rank.length>3 ? 3 : ranktable.Rank.length;

                for(int i=0;i<rankLen;++i) {
                    user_id_text=ranktable.Rank[i].user_id;
                    point_text=Integer.toString(ranktable.Rank[i].point);
                    rankingTxtView[i].setText("    "+Integer.toString(i+1)+", "+user_id_text+" "+point_text);
                }

            }
            @Override
            public void onFailure(Throwable throwable) {
                Log.e("onFailure", throwable.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        PlaySound.PlayTap();
        LinearLayout.LayoutParams licenseScrollViewParam = (LinearLayout.LayoutParams)licenseScrollView.getLayoutParams();
        //ライセンス周りの表示を一度xmlにある初期状態に戻す
        licenseTxtView.setText(getText(R.string.license_view_str));
        for(TextView rankTv : rankingTxtView){
            rankTv.setVisibility(View.VISIBLE);
        }
        licenseScrollViewParam.weight = 0.5f;

        switch (v.getId()){
            case R.id.button_start:
                TextView textView1 = (TextView) findViewById(R.id.text_station);
                //TextView textView2 = (TextView) findViewById(R.id.text_time);
                station_name = textView1.getText().toString();
                //station_time = textView2.getText().toString();

                PlaySound.PlayStart();

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("SName", station_name);

                //intent.putExtra("Stime", station_time);
                Log.d("TAG0",station_name);

                int requestCode = 1000;
                //startActivityForResult(intent, requestCode);
                startActivity(intent);
                break;
            case R.id.license_text:
                //ライセンスを代入する
                licenseTxtView.setText(licenseStr);
                for(TextView rankTv : rankingTxtView){
                    rankTv.setVisibility(View.GONE);
                }
                licenseScrollViewParam.weight = 3.1f;
                break;
        }
        licenseScrollView.setLayoutParams(licenseScrollViewParam);

    }

    @Override
    public void onPause(){
        super.onPause();
        PlaySound.StopMusic();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        PlaySound.ReleaseSe();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            PlaySound.PlayTap();
        }
        return super.onTouchEvent(event);
    }

}
