package jagsc.org.abc2017SpringStation;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by takan on 2017/05/24.
 */

public class EkiSQLiteWrapper {

    public static ArrayList<Integer> getStationsInRange(SQLiteDatabase db, int startStationId, int range){

        ArrayList<Integer> beforeSearch = new ArrayList();
        ArrayList<Integer> afterSearch = new ArrayList();
        beforeSearch.add(startStationId);

        for(int i = 0; i < range; i++){
            ArrayList<Integer> beforeSearchTemp = new ArrayList();
            for(int stationId : beforeSearch){
                Cursor cursor = db.rawQuery("SELECT station_cd, station_g_cd FROM `station` WHERE station_cd = " + stationId +  ";",null);
                int groupCode = 0;
                if (cursor.moveToNext())groupCode = cursor.getInt(1);
                Cursor cursor2 = db.rawQuery("SELECT station_cd, station_g_cd FROM `station` WHERE station_g_cd = " + groupCode +  ";",null);

                try{
                    while (cursor2.moveToNext()){ //SQLの結果毎にループ
                        int station = cursor2.getInt(1);
                        if(!beforeSearch.contains(station) && !afterSearch.contains(station)){
                            beforeSearchTemp.add(station);
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
            for(int stationId : beforeSearch){
                Cursor cursor = db.rawQuery("SELECT line_cd, station_cd1, station_cd2 FROM `join` WHERE station_cd1 = " + stationId +  " OR station_cd2 = " + stationId +  " ;",null);
                try {
                    while (cursor.moveToNext()) { //SQLの結果毎にループ
                        int station1 = cursor.getInt(1);
                        if (!beforeSearch.contains(station1) && !afterSearch.contains(station1) && stationId!=station1) {
                            beforeSearchTemp.add(station1);
                        }
                        int station2 = cursor.getInt(2);
                        if (!beforeSearch.contains(station2) && !afterSearch.contains(station2) && stationId!=station2) {
                            beforeSearchTemp.add(station2);
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
            afterSearch.addAll(beforeSearch);
            beforeSearch = (ArrayList<Integer>) beforeSearchTemp.clone();
        }

        ArrayList<Integer> groupList = new ArrayList();
        ArrayList<Integer> ret = new ArrayList();
        for(int stationId : afterSearch){
            Cursor cursor = db.rawQuery("SELECT station_cd, station_g_cd FROM `station` WHERE station_cd = " + stationId +  ";",null);
            int groupCode = 0;
            try{
                while (cursor.moveToNext()){ //SQLの結果毎にループ
                    if(!groupList.contains(cursor.getInt(1))){
                        groupList.add(cursor.getInt(1));
                        ret.add(cursor.getInt(0));
                    }
                }
            } finally {
                cursor.close();
            }
        }

        return (ArrayList<Integer>) ret.clone();
    }

    public static String getStationsName(SQLiteDatabase db, int stationId){
        Cursor cursor = db.rawQuery(
                "SELECT S.station_cd, S.station_name, P.pref_name " +
                        "FROM station AS S " +
                        "INNER JOIN pref AS P ON S.pref_cd = P.pref_cd " +
                        "WHERE S.station_cd = "+stationId+" ;"
                ,null);
        String ret = "";
        if(cursor.moveToNext()){
            String station_name = cursor.getString(1);
            String pref_name = cursor.getString(2);
            ret = station_name + "(" + pref_name + ")";
        }
        return ret;
    }

    public static LatLng getStationsLocation(SQLiteDatabase db, int stationId){
        Cursor cursor = db.rawQuery(
                "SELECT S.station_cd, S.lon, S.lat " +
                        "FROM station AS S " +
                        "WHERE S.station_cd = "+stationId+" ;"
                ,null);
        LatLng latlng = new LatLng(0,0);
        while (cursor.moveToNext()){ //SQLの結果毎にループ
            latlng = new LatLng(cursor.getDouble(2), cursor.getDouble(1));
        }
        return latlng;
    }
}
